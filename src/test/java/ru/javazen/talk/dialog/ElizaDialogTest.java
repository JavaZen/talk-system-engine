package ru.javazen.talk.dialog;


import org.junit.Before;
import org.junit.Test;
import ru.javazen.talk.analysis.grapheme.GraphemeAnalyzer;
import ru.javazen.talk.analysis.grapheme.impl.OpenNlpGraphemeAnalyzer;
import ru.javazen.talk.analysis.morphology.MorphologyAnalyzer;
import ru.javazen.talk.analysis.morphology.impl.JLanguageToolMorphologyAnalyzer;
import ru.javazen.talk.analysis.morphology.impl.OpenNlpMorphologyAnalyzer;
import ru.javazen.talk.analysis.util.KeywordComparator;
import ru.javazen.talk.analysis.util.Lemmatizer;
import ru.javazen.talk.knowledgebase.Keyword;
import ru.javazen.talk.knowledgebase.KeywordToKeyword;
import ru.javazen.talk.knowledgebase.KeywordToResponse;
import ru.javazen.talk.knowledgebase.Response;
import ru.javazen.talk.knowledgebase.repository.KnowledgeBaseRepository;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ElizaDialogTest {

    private static final String HI_MY_DEAR_FRIEND = "Привет, мой дорогой друг!";
    private static final String HI = "Ку";
    private static final String HELLO = "Привет";

    KnowledgeBaseRepository repository;
    private ElizaDialog dialog = new ElizaDialog();

    private JLanguageToolMorphologyAnalyzer jLanguageToolMorphologyAnalyzer() {
        return new JLanguageToolMorphologyAnalyzer();
    }

    private OpenNlpMorphologyAnalyzer openNlpMorphologyAnalyzer() throws IOException {
        return new OpenNlpMorphologyAnalyzer(OpenNlpMorphologyAnalyzer.getDefaultModelRu());
    }

    private Lemmatizer lemmatizer() throws IOException {
        Lemmatizer lemmatizer = new Lemmatizer();
        lemmatizer.setjLanguageToolMorphologyAnalyzer(jLanguageToolMorphologyAnalyzer());
        lemmatizer.setOpenNlpMorphologyAnalyzer(openNlpMorphologyAnalyzer());
        return lemmatizer;
    }

    private KeywordComparator keywordComparator() throws IOException {
        KeywordComparator comparator = new KeywordComparator();
        comparator.setLemmatizer(lemmatizer());
        return comparator;
    }

    @Before
    public void setup() throws IOException {
        MorphologyAnalyzer morphologyAnalyzer
                = new OpenNlpMorphologyAnalyzer(OpenNlpMorphologyAnalyzer.getDefaultModelRu());
        dialog.setMorphologyAnalyzer(morphologyAnalyzer);

        GraphemeAnalyzer graphemeAnalyzer
                = new OpenNlpGraphemeAnalyzer(OpenNlpGraphemeAnalyzer.getDefaultModelRu());
        dialog.setGraphemeAnalyzer(graphemeAnalyzer);

        dialog.setKeywordComparator(keywordComparator());
        repository = mock(KnowledgeBaseRepository.class);

        Keyword keyword1 = new Keyword();
        keyword1.setId(1L);
        keyword1.setText(HELLO);

        Keyword keyword2 = new Keyword();
        keyword2.setId(2L);
        keyword2.setText(HI);

        KeywordToKeyword keywordToKeyword1 = new KeywordToKeyword();
        keywordToKeyword1.setId(1L);
        keywordToKeyword1.setRelatedFrom(keyword1);
        keywordToKeyword1.setRelatedTo(keyword2);
        keywordToKeyword1.setTypeOfRelation(KeywordToKeyword.Type.SYNONYM);

        Response response1 = new Response();
        response1.setId(1L);
        response1.setText(HI_MY_DEAR_FRIEND);
        response1.setType(Response.Type.REPEATABLE);
        response1.setFlags(Collections.singleton(Response.Flag.ANSWER));

        KeywordToResponse keywordToResponse1 = new KeywordToResponse();
        keywordToResponse1.setId(1L);
        keywordToResponse1.setRelatedFrom(keyword1);
        keywordToResponse1.setRelatedTo(response1);

        when(repository.getAllKeywords()).thenReturn(Arrays.asList(keyword1, keyword2));
        when(repository.getAllResponse()).thenReturn(Arrays.asList(response1));
        when(repository.getAllKeywordToKeywords()).thenReturn(Arrays.asList(keywordToKeyword1));
        when(repository.getAllKeywordToResponses()).thenReturn(Arrays.asList(keywordToResponse1));

        dialog.setKnowledgeBaseRepository(repository);

    }

    @Test
    public void sendReplyTest() {

        Reply reply = new Reply();
        reply.setText(HI);

        dialog.sendReply(reply);

        assertEquals(dialog.obtainReply().getText(), HI_MY_DEAR_FRIEND);

    }


}
