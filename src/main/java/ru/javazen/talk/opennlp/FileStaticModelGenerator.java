package ru.javazen.talk.opennlp;

import ru.javazen.talk.opencorpora.entity.*;
import ru.javazen.talk.opennlp.model.LemmatizerModelGenerator;
import ru.javazen.talk.opennlp.model.ModelGenerator;
import ru.javazen.talk.opennlp.model.SentenceDetectorModelGenerator;
import ru.javazen.talk.opennlp.model.TokenizerModelGenerator;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Andrew on 17.05.2017.
 */
public class FileStaticModelGenerator {

    private static String directory = "D:/nlp/corpus/annot.opcorpora.xml.byfile/";
    private static String sentenceModelPath = "D:/nlp/corpus/sentence.txt";
    private static String tokenizerModelPath = "D:/nlp/corpus/tokenizer.txt";
    private static String lemmatizerModelPath = "D:/nlp/corpus/lemmatizer2.2.txt";

    private static ModelGenerator sentenceGenerator = new SentenceDetectorModelGenerator();
    private static ModelGenerator tokenizerGenerator = new TokenizerModelGenerator();
    private static ModelGenerator lemmatizerGenerator = new LemmatizerModelGenerator();

    public static void main(String[] args) throws IOException {
        File sourceDir = new File(directory);
        if (!sourceDir.isDirectory()) { return; }

        File sentenceModel = new File(sentenceModelPath);
        File tokenizerModel = new File(tokenizerModelPath);
        File lemmatizerModel = new File(lemmatizerModelPath);

        if (!sentenceModel.exists()) {
            sentenceModel.createNewFile();
        }
        if (!tokenizerModel.exists()) {
            tokenizerModel.createNewFile();
        }
        if (!lemmatizerModel.exists()) {
            lemmatizerModel.createNewFile();
        }

        /*for (File source : sourceDir.listFiles()) {

            Text text = fromXmlToObject(source, Text.class);

            try(FileWriter fw = new FileWriter(sentenceModel, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {

                out.print(sentenceGenerator.generate(text));
            }

            try(FileWriter fw = new FileWriter(tokenizerModel, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {

                out.print(tokenizerGenerator.generate(text));
            }

            try(FileWriter fw = new FileWriter(lemmatizerModel, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {

                out.print(lemmatizerGenerator.generate(text));
            }

        }*/
        File annotationFile = new File("D:/nlp/corpus/2/annot.opcorpora.no_ambig.xml/annot.opcorpora.no_ambig.xml");

        Annotation annotation = fromXmlToObject(annotationFile, Annotation.class);


        Set<String> pos = new HashSet<>();

        for (Text text : annotation.getTexts()) {
            /*try(FileWriter fw = new FileWriter(lemmatizerModel, true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw)) {

                out.print(lemmatizerGenerator.generate(text));
            }*/


            List<Paragraph> paragraphs = text.getParagraphs();
            for (Paragraph paragraph : paragraphs) {
                List<Sentence> sentences = paragraph.getSentences();

                for (Sentence sentence : sentences) {
                    for (Token token : sentence.getTokens()) {
                        String p = token.getTfr().getV().getL().getGs().get(0).getV();
                        pos.add(p);
                    }
                }
            }
        }
        pos.forEach(s -> System.out.println(s));
    }

    private static <T> T  fromXmlToObject(File file, Class<T> clazz) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
            Unmarshaller un = jaxbContext.createUnmarshaller();

            return (T) un.unmarshal(file);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
