package ru.javazen.talk.opennlp.model;

import ru.javazen.talk.opencorpora.entity.Paragraph;
import ru.javazen.talk.opencorpora.entity.Sentence;
import ru.javazen.talk.opencorpora.entity.Text;
import ru.javazen.talk.opencorpora.entity.Token;

import java.util.List;

/**
 * Created by Andrew on 17.05.2017.
 */
public class TokenizerModelGenerator implements ModelGenerator {

    @Override
    public String generate(Text text) {
        StringBuilder builder = new StringBuilder();

        List<Paragraph> paragraphs = text.getParagraphs();
        for (Paragraph paragraph : paragraphs) {
            List<Sentence> sentences = paragraph.getSentences();

            for (Sentence sentence : sentences) {
                List<Token> tokens = sentence.getTokens();
                for (int i = 0; i < tokens.size(); i++) {
                    Token token = tokens.get(i);
                    String w = token.getText();

                    builder.append(w);
                    if (i != tokens.size() - 1) {
                        builder.append("<SPLIT>");
                    }
                }
                builder.append("\n");
            }

        }

        return builder.toString();
    }
}
