package ru.javazen.talk.opennlp.model;

import ru.javazen.talk.opencorpora.entity.Paragraph;
import ru.javazen.talk.opencorpora.entity.Sentence;
import ru.javazen.talk.opencorpora.entity.Text;

import java.util.List;

/**
 * Created by Andrew on 17.05.2017.
 */
public class SentenceDetectorModelGenerator implements ModelGenerator {

    @Override
    public String generate(Text text) {
        StringBuilder builder = new StringBuilder();

        List<Paragraph> paragraphs = text.getParagraphs();
        for (Paragraph paragraph : paragraphs) {
            List<Sentence> sentences = paragraph.getSentences();

            for (Sentence sentence : sentences) {
                builder.append(sentence.getSource()).append("\n");
            }
            builder.append("\n"); //todo - maybe empty line peer document?
        }

        return builder.toString();
    }
}