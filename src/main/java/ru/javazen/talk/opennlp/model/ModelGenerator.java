package ru.javazen.talk.opennlp.model;

import ru.javazen.talk.opencorpora.entity.Text;

public interface ModelGenerator {

    String generate(Text text);

}
