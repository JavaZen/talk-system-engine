package ru.javazen.talk.opennlp.model;

import ru.javazen.talk.opencorpora.entity.Paragraph;
import ru.javazen.talk.opencorpora.entity.Sentence;
import ru.javazen.talk.opencorpora.entity.Text;
import ru.javazen.talk.opencorpora.entity.Token;

import java.util.List;

/**
 * Created by Andrew on 18.05.2017.
 */
public class LemmatizerModelGenerator implements ModelGenerator {

    @Override
    public String generate(Text text) {
        StringBuilder builder = new StringBuilder();

        List<Paragraph> paragraphs = text.getParagraphs();
        for (Paragraph paragraph : paragraphs) {
            List<Sentence> sentences = paragraph.getSentences();

            for (Sentence sentence : sentences) {
                for(Token token : sentence.getTokens()) {
                    String w = token.getText();
                    String lemma = token.getTfr().getV().getL().getT();
                    String pos = token.getTfr().getV().getL().getGs().get(0).getV();
                    builder
                            .append(w).append("\t").append(pos)
                            .append("\t").append(lemma).append("\n");
                }
            }

        }

        return builder.toString();
    }
}
