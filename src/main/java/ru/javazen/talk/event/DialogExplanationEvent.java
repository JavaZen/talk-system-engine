package ru.javazen.talk.event;

import ru.javazen.talk.explanation.data.Base;

/**
 * Created by Andrew on 25.05.2017.
 */
public class DialogExplanationEvent implements DialogEvent<Base> {

    private Base explanation;

    public DialogExplanationEvent(Base explanation) {
        this.explanation = explanation;
    }

    @Override
    public Base getEntity() {
        return explanation;
    }
}
