package ru.javazen.talk.event;

/**
 * This interface allows to receive events from dialog
 */
public interface EventSubscriber<T> {

    Class<T> getType();

    void handleDialogEvent(DialogEvent<T> event);
}
