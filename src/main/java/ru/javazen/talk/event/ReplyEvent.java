package ru.javazen.talk.event;

import ru.javazen.talk.dialog.Reply;


public class ReplyEvent implements DialogEvent<Reply> {

    private Reply reply;

    public ReplyEvent(Reply reply) {
        this.reply = reply;
    }

    public Reply getEntity() {
        return reply;
    }

}
