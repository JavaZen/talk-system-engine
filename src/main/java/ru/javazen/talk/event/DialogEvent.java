package ru.javazen.talk.event;

/**
 * Is an event which may occur in the dialog.
 */
public interface DialogEvent<T> {

    T getEntity();
}
