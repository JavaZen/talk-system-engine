package ru.javazen.talk.opencorpora.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Andrew on 17.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Token {

    @XmlAttribute
    private Long id;

    @XmlAttribute
    private String text;

    @XmlElement
    private Tfr tfr;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Tfr getTfr() {
        return tfr;
    }

    public void setTfr(Tfr tfr) {
        this.tfr = tfr;
    }
}
