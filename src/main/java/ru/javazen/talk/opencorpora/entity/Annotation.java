package ru.javazen.talk.opencorpora.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Andrew on 20.05.2017.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Annotation {

    @XmlElement(name = "text")
    private List<Text> texts;

    public List<Text> getTexts() {
        return texts;
    }

    public void setTexts(List<Text> texts) {
        this.texts = texts;
    }
}
