package ru.javazen.talk.opencorpora.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Andrew on 16.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Paragraph {

    @XmlAttribute
    private Long id;

    @XmlElement(name = "sentence")
    private List<Sentence> sentences;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }
}
