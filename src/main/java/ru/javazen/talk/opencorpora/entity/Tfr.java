package ru.javazen.talk.opencorpora.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by Andrew on 17.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Tfr {

    @XmlAttribute(name = "rev_id")
    private Long revId;

    @XmlAttribute
    private String t;

    @XmlElement
    private V v;

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class V {

        @XmlElement
        private L l;

        public L getL() {
            return l;
        }

        public void setL(L l) {
            this.l = l;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class L {

        @XmlAttribute
        private Long id;

        @XmlAttribute
        private String t;

        @XmlElement(name = "g")
        private List<G> gs;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getT() {
            return t;
        }

        public void setT(String t) {
            this.t = t;
        }

        public List<G> getGs() {
            return gs;
        }

        public void setGs(List<G> gs) {
            this.gs = gs;
        }
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    public static class G {

        @XmlAttribute
        private String v;

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }
    }

    public Long getRevId() {
        return revId;
    }

    public void setRevId(Long revId) {
        this.revId = revId;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public V getV() {
        return v;
    }

    public void setV(V v) {
        this.v = v;
    }
}
