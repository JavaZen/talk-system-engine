package ru.javazen.talk.opencorpora.entity;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by Andrew on 16.05.2017.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Sentence {

    @XmlAttribute
    private Long id;

    @XmlElement
    private String source;

    @XmlElementWrapper(name = "tokens")
    @XmlElement(name = "token")
    private List<Token> tokens;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void setTokens(List<Token> tokens) {
        this.tokens = tokens;
    }
}
