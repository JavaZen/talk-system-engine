package ru.javazen.talk.analysis.grapheme;

/**
 * Created by Andrew on 08.05.2017.
 */
public interface GraphemeAnalyzer {

    String[] extractGraphemes(String text);

}
