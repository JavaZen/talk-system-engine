package ru.javazen.talk.analysis.spelling;

import org.languagetool.rules.RuleMatch;

import java.util.List;

/**
 * Created by Andrew on 20.05.2017.
 */
public interface SpellingAnalyzer {

    //todo remove dependency on library and define own API for result
    List<RuleMatch> match(String text);
}
