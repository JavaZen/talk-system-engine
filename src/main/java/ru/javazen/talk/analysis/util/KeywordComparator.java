package ru.javazen.talk.analysis.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.javazen.talk.explanation.DialogExplorer;
import ru.javazen.talk.knowledgebase.Keyword;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Andrew on 22.05.2017.
 */
public class KeywordComparator {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeywordComparator.class);

    private Lemmatizer lemmatizer;

    public boolean compare(Keyword keyword, String word) {
        LOGGER.trace("Start comparing for word '{}' keyword {}", word, keyword);
        // if the words is equals - return true
        if (keyword.getText().equalsIgnoreCase(word) && !isMasked(keyword)) {
            DialogExplorer.explorer().trace("    Найдено совпадение - слова одинаковые");
            LOGGER.trace("Keyword equals by hard comparing");
            return true;
        }

        if (isMasked(keyword)) {
            LOGGER.trace("Start comparing by mask");
            boolean res = false;
            try {
                Pattern pattern = Pattern.compile(keyword.getText(), Pattern.CASE_INSENSITIVE); //TODO here we can implement cache
                Matcher matcher = pattern.matcher(word);
                res =  matcher.matches();
                if (res) {
                    DialogExplorer.explorer().trace("    Найдено совпадение по маске");
                }
            } catch (Exception e) {
                DialogExplorer.explorer().trace("      Ошибка для маски слова: " + keyword.getText());
                LOGGER.warn("Wrong regexp for keyword: {}", keyword, e);
            }

            LOGGER.trace("Result of comparing by mask = '{}'", res);
            return res;
        }

        String firstLemma = lemmatizer.resolveLemma(keyword.getText());
        String secondLemma = lemmatizer.resolveLemma(word);
        if (firstLemma.equalsIgnoreCase(secondLemma)) {
            LOGGER.trace("Keyword equals by lemma comparing");
            DialogExplorer.explorer().trace("    Найдено совпадение по основе слова: " + firstLemma);
            return true;
        }

        LOGGER.trace("Words is not equals");
        return false;
    }

    private boolean isMasked(Keyword keyword) {
        return keyword.getFlags() != null && keyword.getFlags().contains(Keyword.Flag.MASKED);
    }

    public Lemmatizer getLemmatizer() {
        return lemmatizer;
    }

    public void setLemmatizer(Lemmatizer lemmatizer) {
        this.lemmatizer = lemmatizer;
    }
}