package ru.javazen.talk.analysis.morphology;

import java.util.Set;

/**
 * Created by Andrew on 08.05.2017.
 */
public interface MorphologyAnalyzer {

    @Deprecated
    String[] extractLemmas(String[] words);

    @Deprecated
    String extractLemma(String word);

    Set<String> extractLemmas(String word);
}
