package ru.javazen.talk.dialog;

import java.util.Objects;

public class Reply {

    private Long id;

    private String text;

    private Type type;

    public enum Type {
        QUESTION_FROM_USER,
        QUESTION_FROM_SYSTEM,
        ANSWER_FROM_USER,
        ANSWER_FROM_SYSTEM
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Reply{"+
                "id = " + id +
                ", text=" + text +
                ", type=" + type +
                "}";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) { return true; }
        if (obj == null || !(obj instanceof Reply)) { return false; }
        Reply other = (Reply) obj;
        return id.equals(other.getId()) && text.equals(other.getText()) && type == other.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, type);
    }
}
