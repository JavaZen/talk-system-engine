package ru.javazen.talk.dialog;

import ru.javazen.talk.event.DialogEvent;
import ru.javazen.talk.event.EventSubscriber;
import ru.javazen.talk.knowledgebase.repository.KnowledgeBaseRepository;

public interface Dialog {

    Long getId();

    void setId(Long id);

    Reply obtainReply();

    void sendReply(Reply reply);

    void subscribeToEvent(EventSubscriber subscriber);

    <T extends DialogEvent> void subscribeToEvent(EventSubscriber subscriber, Class<T> type);

    Status getStatus();

    void resetContext();

    void setKnowledgeBaseRepository(KnowledgeBaseRepository knowledgeBaseRepository);

    enum Status {
        STARTED,
        IN_PROGRESS,
        WAIT_TO_ANSWER,
        WAIT_TO_REPLY,
        FINISHED
    }
}
