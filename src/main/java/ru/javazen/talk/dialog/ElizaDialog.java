package ru.javazen.talk.dialog;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.javazen.talk.analysis.grapheme.GraphemeAnalyzer;
import ru.javazen.talk.analysis.morphology.MorphologyAnalyzer;
import ru.javazen.talk.analysis.util.KeywordComparator;
import ru.javazen.talk.event.DialogEvent;
import ru.javazen.talk.event.DialogExplanationEvent;
import ru.javazen.talk.event.EventSubscriber;
import ru.javazen.talk.event.ReplyEvent;
import ru.javazen.talk.explanation.DialogExplorer;
import ru.javazen.talk.explanation.data.Base;
import ru.javazen.talk.knowledgebase.*;
import ru.javazen.talk.knowledgebase.repository.CachedRepositoryWrapper;
import ru.javazen.talk.knowledgebase.repository.KnowledgeBaseRepository;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ElizaDialog implements Dialog {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElizaDialog.class);

    private Long id;

    private GraphemeAnalyzer graphemeAnalyzer;

    private MorphologyAnalyzer morphologyAnalyzer;

    private KeywordComparator keywordComparator;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    public GraphemeAnalyzer getGraphemeAnalyzer() {
        return graphemeAnalyzer;
    }

    public void setGraphemeAnalyzer(GraphemeAnalyzer graphemeAnalyzer) {
        this.graphemeAnalyzer = graphemeAnalyzer;
    }

    public MorphologyAnalyzer getMorphologyAnalyzer() {
        return morphologyAnalyzer;
    }

    public void setMorphologyAnalyzer(MorphologyAnalyzer morphologyAnalyzer) {
        this.morphologyAnalyzer = morphologyAnalyzer;
    }

    public KeywordComparator getKeywordComparator() {
        return keywordComparator;
    }

    public void setKeywordComparator(KeywordComparator keywordComparator) {
        this.keywordComparator = keywordComparator;
    }

    private List<Reply> replies = new ArrayList<>();

    private KnowledgeBaseRepository knowledgeBaseRepository;
    private CachedRepositoryWrapper repositoryWrapper;

    private LinkedList<Reply> repliesForReturn = new LinkedList<>();

    private List<Response> sentResponses = new ArrayList<>();

    private List<EventSubscriber> eventSubscribers = new ArrayList<>();

    /**
     * Context response under which we need to search response
     */
    private Long contextResponseId;

    private Response getContextResponse() {
        if (contextResponseId == null) {
            return null;
        }
        Optional<Response> response = repositoryWrapper.getAllResponse()
                .stream().filter(r -> r.getId().equals(contextResponseId)).findFirst();

        if (response.isPresent()) {
            return response.get();
        }
        return null;
    }

    private void setContextResponse(Response contextResponse) {
        this.contextResponseId = contextResponse != null ? contextResponse.getId() : null;
    }

    @Override
    public Reply obtainReply() {
        return repliesForReturn.removeFirst();
    }

    @Override
    public void sendReply(Reply reply) {
        if (graphemeAnalyzer == null || morphologyAnalyzer == null || keywordComparator == null) {
            throw new IllegalStateException("Eliza is not initialized");
        }
        LOGGER.trace("Obtained reply: {}", reply);
        replies.add(reply);

        processReply(reply);
        repositoryWrapper.resetCache();

        sendExplanationToSubscribers();
    }

    private void sentResponseToSubscribers(Response response) {
        sentResponses.add(response);
        if (response.getFlags().contains(Response.Flag.ANSWER) || response.getFlags().contains(Response.Flag.QUESTION)) {
            setContextResponse(response);
        }

        Reply replyForResponse = new Reply();
        replyForResponse.setText(response.getText());
        replyForResponse.setType(Reply.Type.ANSWER_FROM_SYSTEM);

        DialogEvent replyEvent = new ReplyEvent(replyForResponse);
        eventSubscribers.forEach(eventSubscriber -> {
            //TODO
            if (eventSubscriber.getType() == replyEvent.getClass()) {
                eventSubscriber.handleDialogEvent(replyEvent);
            }
        });
        replies.add(replyForResponse);
        repliesForReturn.add(replyForResponse);
    }

    private void sendExplanationToSubscribers() {
        eventSubscribers.forEach(eventSubscriber -> {
            if (eventSubscriber.getType() == DialogExplanationEvent.class) {
                for (Base base : DialogExplorer.explorer().getExplanation()) {
                    eventSubscriber.handleDialogEvent(new DialogExplanationEvent(base));
                }
            }
        });
        DialogExplorer.remove();
    }

    @Override
    public void subscribeToEvent(EventSubscriber subscriber) {
        eventSubscribers.add(subscriber);
    }

    @Override
    public <T extends DialogEvent> void subscribeToEvent(EventSubscriber subscriber, Class<T> type) {
        eventSubscribers.add(subscriber);
    }

    @Override
    public Status getStatus() {
        throw new NotImplementedException();
    }

    @Override
    public void resetContext() {
        contextResponseId = null;
        sentResponses.clear();
    }

    private List<WeightedResponse> findWeightedResponsesForKeywords(List<Keyword> keywords) {
        List<Response> responsesToPhrases = new ArrayList<>();
        List<Response> responses = new ArrayList<>();

        //ищем отклики для ключевых слов
        for (Keyword keyword : keywords) {
            List<Keyword> synonyms = getSynonyms(keyword);
            LOGGER.trace("Found synonyms: {}", synonyms);
            List<Response> responsesToKeyword = getAllResponsesByKeywords(synonyms);
            LOGGER.trace("Responses for synonyms: {}", responsesToKeyword);
            if (responsesToKeyword != null) {
                responses.addAll(responsesToKeyword);
                if (keyword.getFlags() != null && keyword.getFlags().contains(Keyword.Flag.PHRASE)) {
                    responsesToPhrases.addAll(responsesToKeyword);
                }
            }
        }

        LOGGER.trace("Found responses: {}", responses);

        //группируем с учетом совпадений по ключевым словам
        Map<Response, Long> countedResponses = responses
                .stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        //если найдено по фразе - отдается приоритет этому ответу
        responsesToPhrases.forEach(response -> {
            countedResponses.compute(response, (r, c) -> 100L);
        });

        //преобразуем в сущности, которые имеют вес, который представляет количество соврадений по ключевым словам
        List<WeightedResponse> weightedResponses
                = countedResponses.entrySet().stream()
                .collect(Collectors.mapping(cr -> new WeightedResponse(cr.getKey(),
                        cr.getValue().intValue()), Collectors.toList()));

        return weightedResponses;
    }

    /**
     * Execute main logic of Eliza dialog
     */
    private void processReply(Reply reply) {
        DialogExplorer.explorer().startBase();
        LOGGER.trace("Start search Response for reply: {}", reply);
        DialogExplorer.explorer().startDetail();
        //достаем ключевые слова
        List<Keyword> keywords = extractKeywordsFromReply(reply);
        LOGGER.trace("Found keywords: {}", keywords);
        DialogExplorer.explorer().endDetail("Поиск ключевых слов. Найденные ключевые слова: " + keywords);

        DialogExplorer.explorer().startDetail();
        List<WeightedResponse> weightedResponses = findWeightedResponsesForKeywords(keywords);
        DialogExplorer.explorer().endDetail("Поиск откликов. Найденные ассоциированные отклики: " + weightedResponses);

        LOGGER.trace("Context response: {}", getContextResponse());

        //ищем сообщение типа Ответ в данном контексте
        Response response = processAnswer(getContextResponse(), weightedResponses);
        LOGGER.trace("Found answer: {}", response);

        //если найден ответ, отправляем сообщение
        if (response != null) {
            sentResponseToSubscribers(response);
            DialogExplorer.explorer().endBase("Найденный отклик системы: " + response);
        } else {
            DialogExplorer.explorer().endBase("Отклика системы не найдено");
        }


        //если сообщение было найдено в данном контексте
        // и это не вопрос
        // и не терминал
        // то ищем вопрос для пользователя в данном контексте
        if (response != null &&
                !response.getFlags().contains(Response.Flag.QUESTION)
                && !response.getFlags().contains(Response.Flag.TERMINAL)) {
            Response secondResponse = null;
            DialogExplorer.explorer().startBase();

            LOGGER.trace("Start search for question after the response: {}", response);
            //ищем сначала в контексте нашего ответа вопрос
            secondResponse = processQuestion(response);
            LOGGER.trace("Question: {}", secondResponse);

            if (secondResponse != null) {
                sentResponseToSubscribers(secondResponse);
                DialogExplorer.explorer().endBase("Найденный вопрос системы: " + secondResponse);
            }
        }


    }

    private Response processAnswer(Response context, List<WeightedResponse> weightedResponses) {
        LOGGER.trace("Answer search process in context {}", context);
        DialogExplorer.explorer().startDetail();
        Response response = searchResponseInContext(context, Response.Flag.ANSWER, false, weightedResponses);
        LOGGER.trace("Founded answer {}", response);
        if (response != null) {
            setContextResponse(response);
            DialogExplorer.explorer().endDetail("Поиск ответа системы. Надейнный ответ: " + response);
            return response;
        } else {
            DialogExplorer.explorer().endDetail("Ответа системы не найдено");
        }
        List<Response> usedImpasseHandlers = new ArrayList<>();
        DialogExplorer.explorer().startDetail();
        Response cr = getContextResponse();
        for (int i = sentResponses.size() - 1; i >= 0; i--) {
            Response r = sentResponses.get(i);
            Set<Response.Flag> flags = r.getFlags();
            if (flags != null
                    && (flags.contains(Response.Flag.ANSWER) || flags.contains(Response.Flag.QUESTION))
                    && !flags.contains(Response.Flag.IMPASSE_HANDLER)) {
                //тут мы должны сначала запроцессить все обработчики тупиков, потом переключать контекст на уроверь выше.
                //contextResponse = response;
                if (r.equals(cr)) {
                    break; //мы дошли до контекстного сообщения, значит собрали все обработчики тупиков
                }
            } else {
                usedImpasseHandlers.add(r);
            }
        }
        response = processImpasse(cr, usedImpasseHandlers);
        if (response != null) {
            DialogExplorer.explorer().endDetail("Поиск обработчика тупика. Найденный обработчик тупика: " + response);
        } else {
            DialogExplorer.explorer().endDetail("Поиск обработчика тупика. Обработчик тупика не найден.");
        }

        LOGGER.trace("Founded impasse {}", response);
        if (response == null && context != null) { // если не нашли ничего и контекст еще не корневой
            //search relation and check context access

            if (context.getContextBehavior() == Response.ContextBehavior.USE_PARENT_CONTEXT) {

                ResponseToResponse responseToResponse = getParentR2R(getContextResponse());
                setContextResponse(responseToResponse != null ? responseToResponse.getRelatedFrom() : null);
                DialogExplorer.explorer().startDetail();
                DialogExplorer.explorer().trace("Новый контектс: " +  getContextResponse());
                DialogExplorer.explorer().endDetail("Смена контекста на родительский и повторение поиска");
                LOGGER.trace("Change context to parent and retry. New context: {}", getContextResponse());
                Response responseFromParentContext = processAnswer(getContextResponse(), weightedResponses);
                if (responseFromParentContext != null) {
                    return responseFromParentContext;
                }
            } else if (context.getContextBehavior() == Response.ContextBehavior.USE_ROOT_CONTEXT) {
                setContextResponse(null); // null as root context

                DialogExplorer.explorer().startDetail();
                DialogExplorer.explorer().endDetail("Смена контекста на корневой и повторение поиска");
                LOGGER.trace("Change context to root and retry.");
                Response responseFromParentContext = processAnswer(getContextResponse(), weightedResponses);
                if (responseFromParentContext != null) {
                    return responseFromParentContext;
                }
            }
            LOGGER.trace("Context can't be change.");
        }

        return response;
    }

    private Response processQuestion(Response context) {

        LOGGER.trace("Question search process in context {}", context);
        Response response = null;

        List<Response> allResponses = repositoryWrapper.getAllResponse().stream().collect(Collectors.toList());

        DialogExplorer.explorer().startDetail();
        response = searchEqualResponseInContext(context, Response.Flag.QUESTION, true, allResponses);
        if (response != null) {
            DialogExplorer.explorer().endDetail("Найденный вопрос системы: " + response);
        } else {
            DialogExplorer.explorer().endDetail("Вопроса системы не найдено.");
        }
        LOGGER.trace("Founded question {}", response);

        if (response == null && context != null) { // если не нашли ничего и контекст еще не корневой

            if (context.getContextBehavior() == Response.ContextBehavior.USE_PARENT_CONTEXT) {

                ResponseToResponse responseToResponse = getParentR2R(getContextResponse());

                setContextResponse(responseToResponse != null ? responseToResponse.getRelatedFrom() : null);
                DialogExplorer.explorer().startDetail();
                DialogExplorer.explorer().trace("Новый контектс: " +  getContextResponse());
                DialogExplorer.explorer().endDetail("Смена контекста на родительский и повторение поиска");
                LOGGER.trace("Change context to parent and retry. New context: {}", getContextResponse());
                Response responseFromParentContext = processQuestion(getContextResponse());
                if (responseFromParentContext != null) {
                    return responseFromParentContext;
                }
            } else if (context.getContextBehavior() == Response.ContextBehavior.USE_ROOT_CONTEXT) {
                setContextResponse(null); // null as root context
                DialogExplorer.explorer().startDetail();
                DialogExplorer.explorer().endDetail("Смена контекста на корневой и повторение поиска");
                LOGGER.trace("Change context to root and retry.");
                Response responseFromParentContext = processQuestion(getContextResponse());
                if (responseFromParentContext != null) {
                    return responseFromParentContext;
                }
            }
            LOGGER.trace("Context can't be change.");
        }

        return response;
    }

    private Response processImpasse(Response context, List<Response> usedImpasses) {

        LOGGER.trace("Start search impasse handler for context: {}", context);
        LOGGER.trace("Used impasse handler: {}", usedImpasses);
        Response response = null;

        boolean defaultExcludeLogic = context != null
                && context.getContextBehavior() == Response.ContextBehavior.KEEP_CONTEXT;
        if (!defaultExcludeLogic && context != null) {
            DialogExplorer.explorer().trace("Данные обработчки будут исключены, так как использовались в данном контексте: " + usedImpasses);
        }
        // filter - исключаем использованные обработчики тупиков в данном контексте
        List<Response> allResponses = repositoryWrapper
                .getAllResponse().stream().filter(r -> (!usedImpasses.contains(r) || defaultExcludeLogic || context==null)
                        && r.getFlags() != null
                        && r.getFlags().contains(Response.Flag.IMPASSE_HANDLER)).collect(Collectors.toList());

        LOGGER.trace("Available impasse handlers: {}", allResponses);

        response = searchEqualResponseInContext(context, Response.Flag.IMPASSE_HANDLER, false, allResponses);

        LOGGER.trace("Founded impasse handler: {}", response);
        return response;
    }

    /**
     * Process search keywords in reply from user
     * @param reply message from user
     * @return founded keywords
     */
    private List<Keyword> extractKeywordsFromReply(Reply reply) {

        String[] wordsArray = graphemeAnalyzer.extractGraphemes(reply.getText());
        List<String> words = new ArrayList<>(Arrays.asList(wordsArray));

        LOGGER.trace("Split words from reply (count={}): {}", words.size(), words);
        Collection<Keyword> keywords = repositoryWrapper.getAllKeywords();

        StringBuilder filteringProcess = new StringBuilder();

        List<Keyword> result = keywords.stream().filter(keyword -> {
            filteringProcess.append("\nKeyword: ").append(keyword.getText()).append("\n\t Words: [");
            DialogExplorer.explorer().trace("Ключевое слово: " + keyword);
            //TODO
            // мягкий парс фраз от пользователя:
            // происходит сначала разбор шаблонной фразы
            // Затем разбирается разбор фразы от пользователя
            // далее для слов-подстановок синонимы
            // далее для каждого слова ворд-компаратором сравниваем совпадение
            // если все слова совпали по позициям - значит всё прошло успешно.
            if (keyword.getFlags() != null && keyword.getFlags().contains(Keyword.Flag.PHRASE)) {
                LOGGER.trace("Keyword is phrase. {}", keyword);
                if (reply.getText().equalsIgnoreCase(keyword.getText())) {
                    LOGGER.trace("Phrases equals by hard compare");
                    filteringProcess.append(reply.getText()).append("]");
                    return true;
                }
                if (keyword.getFlags().contains(Keyword.Flag.MASKED)) {
                    // by regexp
                    LOGGER.trace("Phrase is mask. Try compare by regexp");
                    try {
                        Pattern pattern = Pattern.compile(keyword.getText(), Pattern.CASE_INSENSITIVE); //TODO here we can implement cache
                        Matcher matcher = pattern.matcher(reply.getText());
                        if (matcher.matches()) {
                            LOGGER.trace("Phrases equals by regexp");
                            filteringProcess.append(reply.getText()).append("]");
                            return true;
                        }
                    } catch (Exception e) {
                        //DialogExplorer.explorer().trace("      Ошибка для маски слова: " + keyword.getText());
                        LOGGER.warn("Wrong regexp for keyword: {}", keyword, e);
                    }
                }
            }

            int wCount = 0;
            int wSize = words.size();
            Iterator<String> wordsIterator = words.iterator();
            while (wordsIterator.hasNext()) {
                String s = wordsIterator.next();

                DialogExplorer.explorer().trace("  Слово: " + s);
                filteringProcess.append(s);
                if (++wCount != wSize) { filteringProcess.append(", "); }

                if (keywordComparator.compare(keyword, s)) {
                    filteringProcess.append("]");
                    wordsIterator.remove();
                    return true;
                }
            }
            filteringProcess.append("]");
            return false;
        }).collect(Collectors.toList());

        LOGGER.trace("Filtering process: {}", filteringProcess);
        return result;

    }

    /**
     * Find all Responses which associated with keywords
     * @param keywords ordered keywords, when first - original keywords
     * @return Responses with weight between original word and the response
     */
    private List<Response> getAllResponsesByKeywords(List<Keyword> keywords) {
        if (keywords == null) {
            return null;
        }

        StringBuilder responseSearching = new StringBuilder("Response searching by keyword process");
        responseSearching.append("\n  Search process for keywords: ").append(keywords);
        Collection<KeywordToResponse> keywordToResponses = repositoryWrapper.getAllKeywordToResponses();

        List<Response> responses = new ArrayList<>();

        for (KeywordToResponse keywordToResponse : keywordToResponses) {
            responseSearching.append("\n\tResponse: ").append(keywordToResponse);
            for (int i = 0; i < keywords.size(); i++) { //I need to have counter as weight between original word and response
                Keyword keyword = keywords.get(i);
                responseSearching.append("\n\t\tKeyword: ").append(keyword).append(" weight = ").append(i);

                if (keyword.equals(keywordToResponse.getRelatedFrom())) {
                    Response response = keywordToResponse.getRelatedTo();

                    DialogExplorer.explorer().trace("  Найдена связь с ключевым словом '" + keywordToResponse.getRelatedFrom() +
                            "' для этого отклика: " + keywordToResponse.getRelatedTo());

                    responseSearching.append("\n\t\t\tFounded response: ").append(response);
                    responses.add(response);
                }
            }
        }
        LOGGER.trace("#getAllResponsesByKeywords {}", responseSearching);
        return responses;

    }

    /**
     * Find all synonyms backward by tree
     * @param keyword keyword for search
     * @return all synonyms backward by tree, include the keyword. Keywords is ordered, when first is original keyword.
     */
    private List<Keyword> getSynonyms(Keyword keyword) {
        if (keyword == null) {
            return null;
        }

        List<Keyword> result = new ArrayList<>();
        result.add(keyword);

        Collection<KeywordToKeyword> keywordToKeywords = repositoryWrapper.getAllKeywordToKeywords();
        StringBuilder searchingProcess = new StringBuilder("Synonym searching process");
        searchingProcess.append("\n  Keyword: ").append(keyword);
        keywordToKeywords.forEach(keywordToKeyword -> {
            searchingProcess.append("\n\tkeywordToKeyword=").append(keywordToKeyword);

            if (keyword.equals(keywordToKeyword.getRelatedTo())
                    && keywordToKeyword.getTypeOfRelation() == KeywordToKeyword.Type.SYNONYM) {
                searchingProcess.append("\n\t\tFounded keyword: ").append(keywordToKeyword.getRelatedFrom());
                        result.addAll(getSynonyms(keywordToKeyword.getRelatedFrom()));
            }
        });
        LOGGER.trace("#getSynonyms {}", searchingProcess);

        return result;
    }

    @Override
    public void setKnowledgeBaseRepository(KnowledgeBaseRepository knowledgeBaseRepository) {
        this.knowledgeBaseRepository = knowledgeBaseRepository;
        this.repositoryWrapper = new CachedRepositoryWrapper(knowledgeBaseRepository);
    }

    private static class WeightedResponse {

        private Response response;
        private int weight;

        public WeightedResponse(Response response, int weight) {
            this.response = response;
            this.weight = weight;
        }

        public Response getResponse() {
            return response;
        }

        public int getWeight() {
            return weight;
        }

        @Override
        public String toString() {
            return "WeightedResponse{" +
                    "response=" + response +
                    ", weight=" + weight +
                    "}";
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) { return true; }
            if (obj == null ||!(obj instanceof WeightedResponse)) { return false; }

            WeightedResponse other = (WeightedResponse) obj;
            return this.weight == other.weight && this.response.equals(other.getResponse());
        }

        @Override
        public int hashCode() {
            return Objects.hash(response, weight);
        }
    }


    /**
     * Process search children responses for given response
     * @param response response
     * @return list of children responses
     */
    private List<Response> getChildrenResponses(Response response) {
        Collection<ResponseToResponse> r2rs = repositoryWrapper.getAllResponseToResponses();
        List<Response> responses = r2rs.stream().filter(r2r -> r2r.getRelatedFrom().equals(response)).collect(Collectors.mapping(ResponseToResponse::getRelatedTo, Collectors.toList()));

        return responses;
    }

    /**
     *Process search parent response for given response
     * @param response response
     * @return parent response or null, if the response is root
     */
    private Response getParentResponse(Response response) {
        if (response == null) { return null; }

        Collection<ResponseToResponse> r2rs = repositoryWrapper.getAllResponseToResponses();
        Optional<ResponseToResponse> r2rOpt =  r2rs.stream().filter(r2r -> r2r.getRelatedTo().equals(response)).findFirst();

        if (r2rOpt.isPresent()) {
            return r2rOpt.get().getRelatedFrom();
        }

        return null;
    }

    private ResponseToResponse getParentR2R(Response response) {
        Collection<ResponseToResponse> responseToResponses = repositoryWrapper.getAllResponseToResponses();

        Optional<ResponseToResponse> responseToResponse = responseToResponses.stream().filter(r2r -> response.equals(r2r.getRelatedTo())).findFirst();
        if (responseToResponse.isPresent()) {
            return responseToResponse.get();
        }
        return null;
    }

    private Response searchEqualResponseInContext(Response contextResponse, Response.Flag hasFlag,
            boolean onlyOneFlag, List<Response> availableResponses) {

        List<WeightedResponse> weightedResponses =
                availableResponses.stream()
                        .collect(Collectors.mapping(r -> new WeightedResponse(r, 0), Collectors.toList()));

        LOGGER.trace("All equal weighted responses: {}", availableResponses);

        return searchResponseInContext(contextResponse, hasFlag, onlyOneFlag, weightedResponses);
    }

    //ищет подходящий отклик среди доступных откликов. Берет лишь те, что соответствуют критерию флага.

    /**
     *
     * @param contextResponse
     * @param hasFlag
     * @param onlyOneFlag
     * @param availableResponses - все совпавшие по ключевым словам и взвешенные
     * @return
     */
    private Response searchResponseInContext(Response contextResponse, Response.Flag hasFlag,
            boolean onlyOneFlag, List<WeightedResponse> availableResponses) {

        LOGGER.trace("start searchResponseInContext with parameters: \n" +
                "contextResponse: {}\n" +
                "hasFlag: {}\n" +
                "onlyOneFlag: {}\n" +
                "availableResponses: {}", contextResponse, hasFlag, onlyOneFlag, availableResponses);

        DialogExplorer.explorer().trace("Контекст: " + contextResponse);

        List<Response> children;

        if (contextResponse != null) {
            children = getChildrenResponses(contextResponse);
        } else {
            Collection<Response> responses = repositoryWrapper.getAllResponse();
            children = responses.stream().filter(response -> getParentResponse(response) == null).collect(Collectors.toList());
        }
        LOGGER.trace("Children responses: {}", children);

        //Доступные в данном контектсе респонсы
        List<WeightedResponse> foundResponses = new ArrayList<>();

        for (Response child : children) {
            //проверяем, доступен ли данный респонс в данном контексте
            Optional<WeightedResponse> owr
                    = availableResponses.stream().filter(wr -> wr.getResponse().equals(child)).findAny();
                    //availableResponses.stream().map(WeightedResponse::getResponse).filter(Predicate.isEqual(child)).count()

            WeightedResponse weightedResponse = null;
            if (owr.isPresent()) {
                weightedResponse = owr.get();
            }
            if (/*availableResponses.contains(child)*/
                    weightedResponse != null
                    && child.getFlags().contains(hasFlag)
                    && (!onlyOneFlag || child.getFlags().size() == 1)) {

                if (isCanBeSaid(weightedResponse.getResponse())) {
                    foundResponses.add(weightedResponse);
                }
            }
        }
        LOGGER.trace("foundResponses: {}", foundResponses);
        DialogExplorer.explorer().trace("Доступные в контексте отклики: " + foundResponses);

        int max = 0;
        for (WeightedResponse foundResponse : foundResponses) {
            if (foundResponse.getWeight() > max) { max = foundResponse.getWeight(); }
        }
        DialogExplorer.explorer().trace("Выбраны будут отклики с максимальным совпадением по ключевым словам.");
        final int maxf = max; //todo...

        List<Response> bestResponses = foundResponses.stream()
                .filter(weightedResponse -> weightedResponse.getWeight() == maxf)
                .collect(Collectors.mapping(WeightedResponse::getResponse, Collectors.toList()));

        DialogExplorer.explorer().trace("Выбранные оклики: " + bestResponses);
        DialogExplorer.explorer().trace("Будет выбран отклик с минимальным количеством использований.");
        Optional<Response> response = bestResponses.stream().min((o1, o2) ->
                (int) (sentResponses.stream().filter(r -> r.equals(o1)).count() -
                        sentResponses.stream().filter(r -> r.equals(o2)).count()));

        if (response.isPresent()) {
            Response rfr = response.get();
            LOGGER.trace("Response for return: {}", rfr);
            DialogExplorer.explorer().trace("Найденный отклик: " + rfr);
            return rfr;
        }
        LOGGER.trace("Nothing found for return");
        DialogExplorer.explorer().trace("Отклика не найдено");
        return null;
    }

    private boolean isCanBeSaid(Response response) {
        Optional<Response> sent = sentResponses.stream().filter(r -> r.getId().equals(response.getId())).findFirst();

        if (response.getType() == Response.Type.NOT_REPEATABLE && sent.isPresent()) {
            //если отклик уже был сказан и не может быть повторен
            DialogExplorer.explorer().trace("Отклик уже был использован и не может быть повторен: " + response);
            return false;
        } else if (response.getType() == Response.Type.NOT_REPEATABLE_IN_CONTEXT && sent.isPresent()) {
            //если отклик уже был сказан и не может быть повторно использоват в контексте
            Response context = getContextResponse();
            if (context == null) {
                //если это рутовый контекст, то отклик точно не может быть использован
                DialogExplorer.explorer().trace("Отклик не может быть использован повтороно в рамках данного контекста " +
                        "(контекст корневой, поэтому отклик всегда не повторяемый): " + response);
                return false;
            }
            //перебираем последние сказанные ответы
            for (int i = sentResponses.size() - 1; i >= 0; i--) {
                Response r = sentResponses.get(i);
                if (response.getId().equals(r.getId())) {
                    //если нашелся отклик, который был сказан, то запрешаем
                    DialogExplorer.explorer().trace("Данный отклик не может быть использован повторно в рамках данного контекта: " + response);
                    return false;
                }
                if (r.getId().equals(context.getId())) {
                    //если мы дощли до контекстного, при этом еще не встретили отклик - разрешаем отклик
                    return true;
                }
            }
        }
        return true;
    }

}
