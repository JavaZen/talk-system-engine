package ru.javazen.talk.explanation.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 24.05.2017.
 */
public class Base {
    private static long ids = 0;

    private Long id;

    private String name;

    private List<Detail> details = new ArrayList<>();

    private Base() {
        id = ids++;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public static Builder getBuilder() {
        return new Base().new Builder();
    }

    public class Builder {

        public Builder name(String name) {
            Base.this.name = name;
            return this;
        }

        public Builder detail(Detail detail) {
            Base.this.details.add(detail);
            return this;
        }

        public Base build() {
            return Base.this;
        }
    }

    @Override
    public String toString() {
        return "Base{" +
                "name='" + name + '\'' +
                ", details=" + details +
                '}';
    }
}
