package ru.javazen.talk.explanation.data;

public class Detail {
    private static long ids = 0L;

    private Long id;
    private String name;
    private Trace trace;

    private Detail() {
        id = ids++;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Trace getTrace() {
        return trace;
    }

    public static Builder getBuilder () {
        return new Detail().new Builder();
    }

    public class Builder {

        public Builder name(String name) {
            Detail.this.name = name;
            return this;
        }

        public Builder trace(Trace trace) {
            Detail.this.trace = trace;
            return this;
        }

        public Detail build() {
            return Detail.this;
        }
    }

    @Override
    public String toString() {
        return "Detail{" +
                "name='" + name + '\'' +
                ", trace=" + trace +
                '}';
    }
}