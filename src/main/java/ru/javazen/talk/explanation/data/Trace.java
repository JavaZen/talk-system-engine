package ru.javazen.talk.explanation.data;

import java.util.ArrayList;
import java.util.List;

public class Trace {
    private static long ids = 0L;

    private Long id;
    private String name;
    private List<String> items = new ArrayList<>();

    private Trace() {
        id = ids++;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<String> getItems() {
        return items;
    }

    public static Builder getBuilder () {
        return new Trace().new Builder();
    }

    public class Builder {

        public Builder name(String name) {
            Trace.this.name = name;
            return this;
        }

        public Builder trace(String item) {
            Trace.this.items.add(item);
            return this;
        }

        public Builder nl() {
            Trace.this.items.add("\n");
            return this;
        }

        public Trace build() {
            return Trace.this;
        }
    }

    @Override
    public String toString() {
        return "Trace{" +
                "name='" + name + '\'' +
                ", items=" + items +
                '}';
    }
}