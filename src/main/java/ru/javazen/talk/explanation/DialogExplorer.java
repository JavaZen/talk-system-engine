package ru.javazen.talk.explanation;

import ru.javazen.talk.explanation.data.Base;
import ru.javazen.talk.explanation.data.Detail;
import ru.javazen.talk.explanation.data.Trace;

import java.util.ArrayList;
import java.util.List;

public class DialogExplorer {

    private static ThreadLocal<DialogExplorer> dialogExplanation = new ThreadLocal<>();

    private List<Base> bases = new ArrayList<>();

    private List<Base.Builder> basesStack = new ArrayList<>();
    private List<Detail.Builder> detailsStack = new ArrayList<>();
    private List<Trace.Builder> traceStack = new ArrayList<>();

    public List<Base> getExplanation() {
        return bases;
    }

    public void startBase() {
        basesStack.add(Base.getBuilder());
    }

    public void endBase(String name) {
        if (basesStack.size() == 0) { return; }

        Base.Builder builder = basesStack.remove(basesStack.size() - 1);
        builder.name(name);
        bases.add(builder.build());
    }

    public void startDetail() {
        detailsStack.add(Detail.getBuilder());
        startTrace();
    }

    public void endDetail(String name) {
        if (detailsStack.size() == 0 || basesStack.size() == 0) { return; }

        endTrace("Детали");
        Detail.Builder builder = detailsStack.remove(detailsStack.size() - 1);
        builder.name(name);
        basesStack.get(basesStack.size() - 1).detail(builder.build());
    }

    private void startTrace() {
        traceStack.add(Trace.getBuilder());
    }

    private void endTrace(String name) {
        if (traceStack.size() == 0 || detailsStack.size() == 0) { return; }

        Trace.Builder builder = traceStack.remove(traceStack.size() - 1);
        builder.name(name);
        detailsStack.get(detailsStack.size() - 1).trace(builder.build());
    }

    public void trace(String trace) {
        if (traceStack.size() != 0) {
            traceStack.get(traceStack.size() - 1).trace(trace);
        }
    }

    public static DialogExplorer explorer() {
        DialogExplorer explanation = dialogExplanation.get();
        if (explanation == null) {
            DialogExplorer explan = new DialogExplorer();
            dialogExplanation.set(explan);
        }
        return dialogExplanation.get();
    }

    public static void remove() {
        dialogExplanation.remove();
    }
}
