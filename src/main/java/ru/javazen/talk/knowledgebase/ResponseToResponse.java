package ru.javazen.talk.knowledgebase;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseToResponse {

    @XmlAttribute
    private Long id;

    @XmlElement(name = "relatedToResponse")
    private Response relatedTo;

    @XmlElement(name = "relatedFromResponse")
    private Response relatedFrom;

    @XmlAttribute
    private Type typeOfRelation;

    @XmlType(name = "r2r_type")
    @XmlEnum
    public enum Type {
        /*KEEP_CONTEXT,
        USE_PARENT_CONTEXT,
        USE_ROOT_CONTEXT*/
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Response getRelatedTo() {
        return relatedTo;
    }

    public void setRelatedTo(Response relatedTo) {
        this.relatedTo = relatedTo;
    }

    public Response getRelatedFrom() {
        return relatedFrom;
    }

    public void setRelatedFrom(Response relatedFrom) {
        this.relatedFrom = relatedFrom;
    }

    public Type getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(Type typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }

    @Override
    public String toString() {
        return "ResponseToResponse{" +
                "id=" + id +
                ", relatedTo" + relatedTo +
                ", relatedFrom" + relatedFrom +
                ", typeOfRelation" + typeOfRelation +
                "}";
    }
}
