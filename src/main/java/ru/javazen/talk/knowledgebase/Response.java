package ru.javazen.talk.knowledgebase;


import javax.xml.bind.annotation.*;
import java.util.Objects;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
public class Response {

    @XmlAttribute
    private Long id;

    private String text;

    @XmlAttribute
    private Type type;

    @XmlAttribute
    private ContextBehavior contextBehavior;

    @XmlElementWrapper(name = "flags")
    @XmlElement(name = "flag")
    private Set<Flag> flags;

    @XmlEnum
    public enum Type {
        REPEATABLE,
        NOT_REPEATABLE,
        NOT_REPEATABLE_IN_CONTEXT
    }

    @XmlEnum
    public enum ContextBehavior {
        KEEP_CONTEXT,
        USE_PARENT_CONTEXT,
        USE_ROOT_CONTEXT
    }

    @XmlType(name = "response_flag")
    @XmlEnum
    public enum Flag {
        ANSWER,
        QUESTION,
        IMPASSE_HANDLER,
        TERMINAL
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public ContextBehavior getContextBehavior() {
        return contextBehavior;
    }

    public void setContextBehavior(ContextBehavior contextBehavior) {
        this.contextBehavior = contextBehavior;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    @Override
    public String toString() {
        return "Response{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", type=" + type +
                ", contextBehavior=" + contextBehavior +
                ", flags=" + flags +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Response)) return false;

        Response response = (Response) o;

        if (getId() != null ? !getId().equals(response.getId()) : response.getId() != null) return false;
        if (getText() != null ? !getText().equals(response.getText()) : response.getText() != null) return false;
        if (getType() != response.getType()) return false;
        if (getContextBehavior() != response.getContextBehavior()) return false;
        return getFlags() != null ? getFlags().equals(response.getFlags()) : response.getFlags() == null;

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, type, contextBehavior, flags);
    }

}
