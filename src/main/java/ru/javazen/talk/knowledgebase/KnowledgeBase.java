package ru.javazen.talk.knowledgebase;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * Created by Andrew on 17.06.2017.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class KnowledgeBase {

    @XmlElementWrapper(name = "keywords")
    @XmlElement(name = "keyword")
    private List<Keyword> keywords;

    @XmlElementWrapper(name = "responses")
    @XmlElement(name = "response")
    private List<Response> responses;

    @XmlElementWrapper(name = "r2rs")
    @XmlElement(name = "r2r")
    private List<ResponseToResponse> responseToResponses;

    @XmlElementWrapper(name = "k2ks")
    @XmlElement(name = "k2k")
    private List<KeywordToKeyword> keywordToKeywords;

    @XmlElementWrapper(name = "k2rs")
    @XmlElement(name = "k2r")
    private List<KeywordToResponse> keywordToResponses;

    public List<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }

    public List<Response> getResponses() {
        return responses;
    }

    public void setResponses(List<Response> responses) {
        this.responses = responses;
    }

    public List<ResponseToResponse> getResponseToResponses() {
        return responseToResponses;
    }

    public void setResponseToResponses(List<ResponseToResponse> responseToResponses) {
        this.responseToResponses = responseToResponses;
    }

    public List<KeywordToKeyword> getKeywordToKeywords() {
        return keywordToKeywords;
    }

    public void setKeywordToKeywords(List<KeywordToKeyword> keywordToKeywords) {
        this.keywordToKeywords = keywordToKeywords;
    }

    public List<KeywordToResponse> getKeywordToResponses() {
        return keywordToResponses;
    }

    public void setKeywordToResponses(List<KeywordToResponse> keywordToResponses) {
        this.keywordToResponses = keywordToResponses;
    }
}
