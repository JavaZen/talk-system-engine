package ru.javazen.talk.knowledgebase;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class KeywordToKeyword {

    @XmlAttribute
    private Long id;

    @XmlElement(name = "relatedToKeyword")
    private Keyword relatedTo;

    @XmlElement(name = "relatedFromKeyword")
    private Keyword relatedFrom;

    @XmlAttribute
    private Type typeOfRelation;

    @XmlType(name = "k2k_type")
    @XmlEnum
    public enum Type {
        SYNONYM,
        OTHER
    }

    public Keyword getRelatedTo() {
        return relatedTo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRelatedTo(Keyword relatedTo) {
        this.relatedTo = relatedTo;
    }

    public Keyword getRelatedFrom() {
        return relatedFrom;
    }

    public void setRelatedFrom(Keyword relatedFrom) {
        this.relatedFrom = relatedFrom;
    }

    public Type getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(Type typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }

    @Override
    public String toString() {
        return "KeywordToKeyword{" +
                "id=" + id +
                ", relatedTo=" + relatedTo +
                ", relatedFrom=" + relatedFrom +
                ", typeOfRelation=" + typeOfRelation +
                "}";
    }
}
