package ru.javazen.talk.knowledgebase;


import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
public class KeywordToResponse {

    @XmlAttribute
    private Long id;

    @XmlElement(name = "relatedToResponse")
    private Response relatedTo;

    @XmlElement(name = "relatedFromKeyword")
    private Keyword relatedFrom;

    @XmlAttribute
    private Type typeOfRelation;

    @XmlType(name = "k2r_type")
    @XmlEnum
    public enum Type {
        /*KEEP_CONTEXT,
        USE_PARENT_CONTEXT,
        USE_ROOT_CONTEXT*/
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Response getRelatedTo() {
        return relatedTo;
    }

    public void setRelatedTo(Response relatedTo) {
        this.relatedTo = relatedTo;
    }

    public Keyword getRelatedFrom() {
        return relatedFrom;
    }

    public void setRelatedFrom(Keyword relatedFrom) {
        this.relatedFrom = relatedFrom;
    }

    public Type getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(Type typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }

    @Override
    public String toString() {
        return "KeywordToResponse{" +
                "id=" + id +
                ", relatedTo" + relatedTo +
                ", relatedFrom" + relatedFrom +
                ", typeOfRelation" + typeOfRelation +
                "}";
    }
}
