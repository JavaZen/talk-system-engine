package ru.javazen.talk.knowledgebase.repository;

import ru.javazen.talk.knowledgebase.*;

import java.util.Collection;

/**
 * This class should be provide keyword and response for talk engine.
 *
 * All works with words and responses should be do via the repository.
 */
public interface KnowledgeBaseRepository {

    Collection<Keyword> getAllKeywords();

    Collection<Response> getAllResponse();

    Collection<KeywordToKeyword> getAllKeywordToKeywords();

    Collection<KeywordToResponse> getAllKeywordToResponses();

    Collection<ResponseToResponse> getAllResponseToResponses();

    Keyword saveKeyword(Keyword keyword);

    Response saveResponse(Response response);

    KeywordToKeyword saveKeywordToKeyword(KeywordToKeyword keywordToKeyword);

    KeywordToResponse saveKeywordToResponse(KeywordToResponse keywordToResponse);

    ResponseToResponse saveResponseToResponse(ResponseToResponse responseToResponse);

    boolean removeKeyword(Keyword keyword);

    boolean removeResponse(Response response);

    boolean removeKeywordToKeyword(KeywordToKeyword keywordToKeyword);

    boolean removeKeywordToResponse(KeywordToResponse keywordToResponse);

    boolean removeResponseToResponse(ResponseToResponse responseToResponse);

}