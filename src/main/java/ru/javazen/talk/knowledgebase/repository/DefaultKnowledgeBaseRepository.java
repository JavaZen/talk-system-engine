package ru.javazen.talk.knowledgebase.repository;

import ru.javazen.talk.knowledgebase.*;

import java.util.List;

public class DefaultKnowledgeBaseRepository implements KnowledgeBaseRepository {

    private KnowledgeBase knowledgeBase;

    public void setKnowledgeBase(KnowledgeBase knowledgeBase) {
        this.knowledgeBase = knowledgeBase;
    }

    @Override
    public List<Keyword> getAllKeywords() {
        return knowledgeBase.getKeywords();
    }

    @Override
    public List<Response> getAllResponse() {
        return knowledgeBase.getResponses();
    }

    @Override
    public List<KeywordToKeyword> getAllKeywordToKeywords() {
        return knowledgeBase.getKeywordToKeywords();
    }

    @Override
    public List<KeywordToResponse> getAllKeywordToResponses() {
        return knowledgeBase.getKeywordToResponses();
    }

    @Override
    public List<ResponseToResponse> getAllResponseToResponses() {
        return knowledgeBase.getResponseToResponses();
    }

    @Override
    public Keyword saveKeyword(Keyword keyword) {
        return null;
    }

    @Override
    public Response saveResponse(Response response) {
        return null;
    }

    @Override
    public KeywordToKeyword saveKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        return null;
    }

    @Override
    public KeywordToResponse saveKeywordToResponse(KeywordToResponse keywordToResponse) {
        return null;
    }

    @Override
    public ResponseToResponse saveResponseToResponse(ResponseToResponse responseToResponse) {
        return null;
    }

    @Override
    public boolean removeKeyword(Keyword keyword) {
        return false;
    }

    @Override
    public boolean removeResponse(Response response) {
        return false;
    }

    @Override
    public boolean removeKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        return false;
    }

    @Override
    public boolean removeKeywordToResponse(KeywordToResponse keywordToResponse) {
        return false;
    }

    @Override
    public boolean removeResponseToResponse(ResponseToResponse responseToResponse) {
        return false;
    }
}
