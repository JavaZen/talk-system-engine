package ru.javazen.talk.knowledgebase.repository;

import ru.javazen.talk.knowledgebase.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * In-memory based repository for Knowledge Base
 */
public class InMemoryKnowledgeBaseRepository implements KnowledgeBaseRepository {

    private long keywordsSeq, responsesSeq, keywordToKeywordsSeq, keywordToResponsesSeq, responseToResponsesSeq;

    private Set<Keyword> keywords = new HashSet<>();
    private Set<Response> responses = new HashSet<>();
    private Set<KeywordToKeyword> keywordToKeywords = new HashSet<>();
    private Set<KeywordToResponse> keywordToResponses = new HashSet<>();
    private Set<ResponseToResponse> responseToResponses = new HashSet<>();

    @Override
    public Collection<Keyword> getAllKeywords() {
        return new HashSet<>(keywords);
    }

    @Override
    public Collection<Response> getAllResponse() {
        return new HashSet<>(responses);
    }

    @Override
    public Collection<KeywordToKeyword> getAllKeywordToKeywords() {
        return new HashSet<>(keywordToKeywords);
    }

    @Override
    public Collection<KeywordToResponse> getAllKeywordToResponses() {
        return new HashSet<>(keywordToResponses);
    }

    @Override
    public Collection<ResponseToResponse> getAllResponseToResponses() {
        return new HashSet<>(responseToResponses);
    }

    @Override
    public Keyword saveKeyword(Keyword keyword) {
        if (keyword.getId() == null) {
            keyword.setId(++keywordsSeq);
        }
        keywords.add(keyword);
        return keyword;
    }

    @Override
    public Response saveResponse(Response response) {
        if (response.getId() == null) {
            response.setId(++responsesSeq);
        }
        responses.add(response);
        return response;
    }

    @Override
    public KeywordToKeyword saveKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        if (keywordToKeyword.getId() == null) {
            keywordToKeyword.setId(++keywordToKeywordsSeq);
        }
        keywordToKeywords.add(keywordToKeyword);
        return keywordToKeyword;
    }

    @Override
    public KeywordToResponse saveKeywordToResponse(KeywordToResponse keywordToResponse) {
        if (keywordToResponse.getId() == null) {
            keywordToResponse.setId(++keywordToResponsesSeq);
        }
        keywordToResponses.add(keywordToResponse);
        return keywordToResponse;
    }

    @Override
    public ResponseToResponse saveResponseToResponse(ResponseToResponse responseToResponse) {
        if (responseToResponse.getId() == null) {
            responseToResponse.setId(++responseToResponsesSeq);
        }
        responseToResponses.add(responseToResponse);
        return responseToResponse;
    }

    @Override
    public boolean removeKeyword(Keyword keyword) {
        return keywords.remove(keyword);
    }

    @Override
    public boolean removeResponse(Response response) {
        return responses.remove(response);
    }

    @Override
    public boolean removeKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        return keywordToKeywords.remove(keywordToKeyword);
    }

    @Override
    public boolean removeKeywordToResponse(KeywordToResponse keywordToResponse) {
        return keywordToResponses.remove(keywordToResponse);
    }

    @Override
    public boolean removeResponseToResponse(ResponseToResponse responseToResponse) {
        return responseToResponses.remove(responseToResponse);
    }

}