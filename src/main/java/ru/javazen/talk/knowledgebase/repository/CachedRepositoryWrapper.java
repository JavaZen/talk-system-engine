package ru.javazen.talk.knowledgebase.repository;

import ru.javazen.talk.knowledgebase.*;

import java.util.Collection;

/**
 * Created by Andrew on 12.06.2017.
 */
public class CachedRepositoryWrapper implements KnowledgeBaseRepository {

    private KnowledgeBaseRepository knowledgeBaseRepository;

    public CachedRepositoryWrapper(KnowledgeBaseRepository knowledgeBaseRepository) {
        this.knowledgeBaseRepository = knowledgeBaseRepository;
    }

    ThreadLocal<Collection<Keyword>> keywords = new ThreadLocal<>();
    ThreadLocal<Collection<Response>> responses = new ThreadLocal<>();
    ThreadLocal<Collection<ResponseToResponse>> responseToResponses = new ThreadLocal<>();
    ThreadLocal<Collection<KeywordToResponse>> keywordToResponses = new ThreadLocal<>();
    ThreadLocal<Collection<KeywordToKeyword>> keywordsToKeywords = new ThreadLocal<>();

    public void resetCache() {
        keywords.remove();
        responses.remove();
        responseToResponses.remove();
        keywordToResponses.remove();
        keywordsToKeywords.remove();
    }


    @Override
    public Collection<Keyword> getAllKeywords() {
        if (keywords.get() == null) {
            keywords.set(knowledgeBaseRepository.getAllKeywords());
        }
        return keywords.get();
    }

    @Override
    public Collection<Response> getAllResponse() {
        if (responses.get() == null) {
            responses.set(knowledgeBaseRepository.getAllResponse());
        }
        return responses.get();
    }

    @Override
    public Collection<KeywordToKeyword> getAllKeywordToKeywords() {
        if (keywordsToKeywords.get() == null) {
            keywordsToKeywords.set(knowledgeBaseRepository.getAllKeywordToKeywords());
        }
        return keywordsToKeywords.get();
    }

    @Override
    public Collection<KeywordToResponse> getAllKeywordToResponses() {
        if (keywordToResponses.get() == null) {
            keywordToResponses.set(knowledgeBaseRepository.getAllKeywordToResponses());
        }
        return keywordToResponses.get();
    }

    @Override
    public Collection<ResponseToResponse> getAllResponseToResponses() {
        if (responseToResponses.get() == null) {
            responseToResponses.set(knowledgeBaseRepository.getAllResponseToResponses());
        }
        return responseToResponses.get();
    }

    @Override
    public Keyword saveKeyword(Keyword keyword) {
        return null;
    }

    @Override
    public Response saveResponse(Response response) {
        return null;
    }

    @Override
    public KeywordToKeyword saveKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        return null;
    }

    @Override
    public KeywordToResponse saveKeywordToResponse(KeywordToResponse keywordToResponse) {
        return null;
    }

    @Override
    public ResponseToResponse saveResponseToResponse(ResponseToResponse responseToResponse) {
        return null;
    }

    @Override
    public boolean removeKeyword(Keyword keyword) {
        return false;
    }

    @Override
    public boolean removeResponse(Response response) {
        return false;
    }

    @Override
    public boolean removeKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        return false;
    }

    @Override
    public boolean removeKeywordToResponse(KeywordToResponse keywordToResponse) {
        return false;
    }

    @Override
    public boolean removeResponseToResponse(ResponseToResponse responseToResponse) {
        return false;
    }
}
