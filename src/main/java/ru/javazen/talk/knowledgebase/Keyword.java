package ru.javazen.talk.knowledgebase;


import javax.xml.bind.annotation.*;
import java.util.Objects;
import java.util.Set;

@XmlAccessorType(XmlAccessType.FIELD)
public class Keyword {

    @XmlAttribute
    private Long id;

    private String text;

    @XmlElementWrapper(name = "flags")
    @XmlElement(name = "flag")
    private Set<Flag> flags;

    @XmlType(name = "keyword_flag")
    @XmlEnum
    public enum Flag {
        MASKED,
        PHRASE
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    @Override
    public String toString() {
        return "Keyword{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", flags=" + flags +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Keyword)) return false;

        Keyword keyword = (Keyword) o;

        if (getId() != null ? !getId().equals(keyword.getId()) : keyword.getId() != null) return false;
        if (getText() != null ? !getText().equals(keyword.getText()) : keyword.getText() != null) return false;
        return getFlags() != null ? getFlags().equals(keyword.getFlags()) : keyword.getFlags() == null;

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, flags);
    }
}
